let users = [

{
	name: "Mike Shell",
	username: "mikeBlueShell",
	email: "mikeyShell01@gmail.com",
	password: "iammikey1999",
	isAtive: true,
	dateJoined: "August 8, 2011"
},
{
	name: "Jake Janella",
	username: "jjnella99",
	email: "jakejanella_99@gmail.com",
	password: "jakiejake12",
	isAtive: true,
	dateJoined: "January 14, 2015"
}

];



// Much like a regular array, we can actually access this array of objects the same way.

users[0].email = "mikeKingOfShells@gmail.com"
users[1].email = "janellajakeArchitect@gmail.com"

console.log(users[0].email)
console.log(users[1].email)

// Can we also use array methods for array of objects?

users.push({

	name: "James Jamesson",
	username: "iHateSpidey",
	email: "jamesJjameson@gmail.com",
	password: "spideyisamenace64",
	isAtive: true,
	dateJoined: "February 14, 2000"
});


console.log(users[2])

class User {
	constructor(name,username,email,password){
		this.name = name;
		this.username = username;
		this.email = email;
		this.password  = password;
		this.isACtive = true;
		this.dateJoined = "Septermber 28, 2021";
	}
}

let newUser1 = new User("Kate Middletown","notTheDuchess","seriouslyNotDuchess@gmail.com","notRoyaltyAtAll");
users.push(newUser1)
console.log(newUser1);
console.log(users)

// find() - is able to return the found item
function login(username,password){

	// check if the username does exist or any user uses that user name
	// check if the password given matches the password of our user in the array.

	let userFound = users.find((user)=>{
		return user.username === username && user.password === password
});

	if(userFound){
		alert(`Thank you for logging in, ${userFound.name}`)
	} else
	alert("Login Failed. Invalid Credentials.")

}



let courses = [];
const postCourse = (id,name,description,price) => {

	courses.push({
		id:id,
		name:name,
		description:description,
		price:price,
		isActive:true
	})

	alert(`You have create ${name}. The price is ${price}`)

}

const getSingleCourse =  (id) =>{

	let findCourse = courses.find((course)=> id === course.id);

	return (findCourse);
}

const deleteCourse = () => courses.pop();