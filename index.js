let assets =  `[
		{
		"id":"1",
		"name":"wood",
		"description":"sturdy wood",
		"stock": 15,
		"isAvailable": true,
		"dateAdded": "September 28, 2021"
		},
		{
		"id":"2",
		"name":"tissue",
		"description":"Cute tissue",
		"stock": 20,
		"isAvailable": true,
		"dateAdded": "September 29, 2021"
		}
]`;

// JSON - Javascript Object Notation

	//JSON is a string but formatted as JS objects.
	//JSON is popularly used to pass data from one application to another.
	//JSON is not only used in Javascript but also in other programming languages to pass data.
	//This  is why it is specified as Javascript Object Notation.
	//There are JSON that are saved in  a file with extension called .json
	//There is  a way to turn JSON as JS objects and there a way to turn JS objects to JSON


	// JS objects are not the same as JSON.
		// JSON is a string.
		// JS object is a objects.
		// JSON keys are sorrounded by double qoutes ("")


		/*
	Synxtax for JSON
		{
			"key1": "valueA",
			"key2": "valueB"
		}

		*/

	//Converting JS objects into  JSON
		// This will  allow us to convert/turn JS objects into JSON
		// This is how we are going to manipulate our JSON
		// By first turning our JS objects into JSON, we can also turn JSON back into JS objects.
		// This is commonly used when trying to pass data from one application to another via the use of HTTP requests. HTTPS requests are request ofr resource between a server  and a  client(browser).
		
		// JSON format is also used in databases.

		let batches = [{batchName: 'Batch123'},{batchName:'Batch125'}];
		console.log(JSON.stringify(batches));//JSON Format String

		let data = JSON.stringify({

			name:  "Katniss",
			age: 20,
			address:{
				city: "Kansas",
				state: "Kansas"
			}
		})

		console.log(data)

		//Convert stringified JSON into JS objects

		let batchesJSON = `[
		{
			"batchName": "Batch123"
		},
		{
			"batchName": "Batch125"
		}

		]`;

		console.log(JSON.parse(batchesJSON));

		// JSON.parse() turns JSON into JS objectss
		// turn data from JSON to JS objects again
		let dataJS = console.log(JSON.parse(data));

		// JSON.parse will not mutate/change the original variable. Instead it will return the parsed data.
		console.log(data)

		// batches is originally a JS array turned into JSON format string. JSON.stringify() also does not mutate/change the original variable instead it will return the JSON stringified data.
		console.log(batches)


		
		//Mini Activity

		let members= [
		{
			username: "Saitama",
			password: "onepuuuunch"
		},
		{
			username: "Light Yagami",
			password: "notkira2007"
		},
		{
			username: "L Lawliett",
			password: "lightyagamiiskira2007"
		}
		]

		let JSONarray = JSON.stringify(members);
		console.log(JSONarray)

		let JSONparsed  = JSON.parse(JSONarray);
		console.log(JSONparsed)
		//Can you still use JS array methods on JSON arrays?
		//No. JSON is a string type format.
		/*JSONarray.push({
			username: "Yoh Asakura",
			password: "shamanKingButNotReally"
		})

		console.log(JSONarray)*/
		JSONparsed.push(
			{
			username: "Yoh Asakura",
			password: "xlawsaresecretlyWeak"
		})
		console.log(JSONparsed)

		JSONarray = JSON.stringify(JSONparsed);
		console.log(JSONarray)